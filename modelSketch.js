/*

	App: Booking System API

	Scenario:
		A course booking system application where a user can enroll into a course

	Type: Course Booking System (Web App)

	Description:

		A course booking system application where a user can enroll into a course
		Allows an admin to do CRUD operations
		Allow users to register into our database

	Features:

		- User Registration
		- User Authentication (User login)

		Customer/Authenticated Users:
		- View Courses (All active courses)
		- Enroll Course

		Admin Users:
		- Add Course
		- Update Course
		- Archive/Unarchive a course (soft delete/reactivate the course)
		- View Courses (All courses active/inactive)
		- View/Manage User Accounts

		All Users (Guest, Customers, Admin)
		- View Active Course

*/

// Data Model for the booking system
// Two-way Embedding

/*
	user{
		
		id - unique identifier for the documents,
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin,
		enrollments:[

			id - document identifier,
			courseId - unique identifier for course documents,
			courseName - optional,
			status,
			dateEnrolled - optional
		]

	}

*/

/*
	course{
	
		id - unique identifier for the documents,
		name,
		description,
		price,
		isActive,
		createdOn
		enrollees:[

			id - document identifier,
			userId,
			isPaid,
			dateEnrolled - optional
		]
	}

*/