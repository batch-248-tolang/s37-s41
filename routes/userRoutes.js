const express = require ("express");
const router = express.Router();

const userController = require("../controllers/userControllers")

const auth = require("../auth")

// Route for checking if the user's email already exists in the db
// invoke the checkEmailExists function from the controller file later to communicate with our db
// passes the "body" property of our "request" object to the corresponding controller function 
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})

// route for user registration

router.post("/register",(req,res)=>{
	
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});

// route for user authentication

router.post("/login",(req,res)=>{

	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
})

// [s38 ACTIVITY SECTION START]
router.get("/details",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
})
// [s38 ACTIVITY SECTION END]

// Route to enroll a user to a course

router.post("/enroll",auth.verify,(req,res)=>{

	let data = {

		courseId: req.body.courseId,
		userId: auth.decode(req.headers.authorization).id
	}
	userController.enroll(data).then(resultFromController=>res.send(resultFromController));
})

// allows us to export the router object that will be accessed in our "index.js"

module.exports = router;