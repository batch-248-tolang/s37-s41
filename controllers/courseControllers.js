const Course = require("../models/Course")
/*
module.exports.addCourse = (reqBody) =>{
	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});
	return newCourse.save().then((course,error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})
}
*/
module.exports.addCourse = (data) =>{

	if(data.isAdmin){
		let newCourse = new Course({

			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});
		return newCourse.save().then((course,error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	}else{
		return false
	}

};


// Retrieve all courses
/*
	Steps:
	1. Retieve all the courses from the database

*/

module.exports.getAllCourses = () =>{

	return Course.find({}).then(result=>{
		return result;
	});
};

// Retrieve all active courses
/*
	Step:
	1. Retrieve all the courses from the db with the property "isActive" with the value true
*/

module.exports.getAllActive = () =>{

	return Course.find({isActive: true}).then(result=>{
		return result;
	});
};

// Update a course
/*
	Step
	1. Create a variable "updateCourse" which will contain information retrieved from the reqBody
	2. Find and update the course using the courseId retrieved from the reqParams property and the variable "updateCourse" containing the information from the reqBody
*/

module.exports.updateCourse = (reqParams, reqBody) =>{

	let updatedCourse = {

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error)=>{

		if(error){
			return false
		}else{
			return true;
		};
	});
};

module.exports.archiveCourse = (reqParams, reqBody) =>{

	let archivedCourse = {

		isActive: false
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error)=>{

		if(error){
			return false
		}else{
			return true;
		};
	});
};

module.exports.unarchiveCourse = (reqParams, reqBody) =>{

	let unarchivedCourse = {

		isActive: true
	}

	return Course.findByIdAndUpdate(reqParams.courseId, unarchivedCourse).then((course, error)=>{

		if(error){
			return false
		}else{
			return true;
		};
	});
};