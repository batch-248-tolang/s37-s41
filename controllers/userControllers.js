// use User Model
const User = require("../models/User");
const Course = require("../models/Course");

const bcrypt = require("bcrypt");

const auth = require("../auth");


// Check if the email already exists
/*
		Steps:
		1. Use mongoose "find" method to find a duplicate email
		2. Use the "then" method to send a response back to the front end based on the result of the "find" method

*/

module.exports.checkEmailExists = (reqBody) =>{

	// the result is sent back to the front end via the "then" method found in the route file

	return User.find({email:reqBody.email}).then(result=>{

		// the find method returns a record if a match is found
		if(result.length>0){
			return true;
		// no duplicate email found
		// user is not yet registered in the db
		}else{
			return false
		}
	})
};

// User Registration
/*
	Steps:
	1. create a new User object using the mongoose model and the information from the reqBody
	2. make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (reqBody) =>{

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)

	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})
}

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) =>{

	return User.findOne({email: reqBody.email}).then(result=>{

		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
}

// [s38 ACTIVITY SECTION START]
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result=>{
		result.password = "";
		return result;
	})
}

// [s38 ACTIVITY SECTION END]

// Enroll user to a course
/*
	Steps
	1. Find the document in the db using the User's ID
	2. Add the course Id to the user's enrollment array
	3. Update the document in the MongoDB Atlas
*/

// async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user=>{

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user,error)=>{
			if(error){
				return false
			}else{
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{

		course.enrollees.push({userId: data.userId});

		return course.save().then((course,error)=>{
			if(error){
				return false;
			}else{
				return true
			};
		});
	});

	if(isUserUpdated && isCourseUpdated){
		return true;
	}else{
		return true
	};

}